## WDC028-35

### Code-along for Booking System API

1. Repeat the same steps as the previous session to create an Express JS API.

2. On top of express, nodemon, and mongoose, also install the following packages:
    - bcrypt (for password encryption)
    - cors (for allowing cross-origin resource sharing)
    - jsonwebtoken (for authenticating client-side requests)

3. Inside index.js (our app entry point), require the app dependencies, connect to MongoDB and have the app listen on the designated port.

4. Organize our project by having directories for models, controllers, and routes.

5. Create a user.js file in each of the models, controllers, and routes subdirectories.

6. Define the model for the users in the respective user.js file in the models subdirectory.

7. Define the controller for the users in the controllers subdirectory, importing the corresponding model.

8. Create an auth.js file in the root directory of the project that will contain the logic for authorization via tokens.

9. In the user.js file in routes, import the controller and associate its methods with the user routes, incorporating the functions defined in the auth.js module as middleware on routes that require authorization.

10. Back in the index.js file, import the user route and assign it to the /users endpoint.

### Activity
1. Have the students replicate the steps in defining the model, controller, and route for courses. 