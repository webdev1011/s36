const bcrypt = require('bcrypt');
const { OAuth2Client } = require('google-auth-library');
const User = require('../models/user');
const Course = require('../models/course');
const auth = require('../auth');
const clientId = '365470860275-h0eu9rsjnj7qa7f41s0s197qn9l4sfjh.apps.googleusercontent.com';

module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10),
        loginType: 'email'
	})

	return user.save().then((user, err) => {
		return (err) ? false : true
	})
}

module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then(user => {
		if (user === null) {
			return { error: 'does-not-exist'}
		}
		if (user.loginType !== 'email'){
			return { error: 'login-type-error'}
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if(isPasswordMatched){
			return { accessToken: auth.createAccessToken(user.toObject())}
		} else {
			return { error: 'incorrect-password'}
		}
	})
}

module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}

module.exports.enroll = (params) => {
	return User.findById(params.userId).then(user => {
		user.enrollments.push({ courseId: params.courseId })

		return user.save().then((user, err) => {
			return Course.findById(params.courseId).then(course => {
				course.enrollees.push({ userId: params.userId })

				return course.save().then((course, err) => {
					return (err) ? false : true
				})
			})
		})
	})
}

module.exports.updateDetails = (params) => {
	
}

module.exports.changePassword = (params) => {
	
}

module.exports.verifyGoogleTokenId = async (tokenId) => {

	const client = new OAuth2Client(clientId);
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	})

	console.log(data.payloa.email_verified);

	return true
	if(data.payload.email_verified === true) {

		const user = await User.findOne({ email: data.payload.email});
	
	}

};