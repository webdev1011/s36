import PropTypes from 'prop-types';
import { Card, Button } from 'react-bootstrap';

export default function Course({course}) {

    // Destructure the data prop into separate variables
    const {name, description, price, start_date, end_date} = course;

    function enroll(){
        console.log(`Thank you for enrolling in ${name}`);
    }

    return (

        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text>
                    <span className="subtitle">Description:</span>
                    <br />
                    {description}
                    <br />
                    <span className="subtitle">Price: </span>
                    PhP {price}
                    <br />
                    <span className="subtitle">Start Date: </span>
                    {start_date}
                    <br />
                    <span className="subtitle">End Date: </span>
                    {end_date}
                </Card.Text>

                <Button variant="primary" onClick={enroll}>Enroll</Button>
                
            </Card.Body>
        </Card>

    )
}

// Checks if the Course component is getting the correct prop types
Course.propTypes = {
    // shape() is used to check that a prop object conforms to a specific "shape" or data structure
    course: PropTypes.shape({
        //define the properties and their expected types
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        start_date: PropTypes.string.isRequired,
        end_date: PropTypes.string.isRequired
    })
}