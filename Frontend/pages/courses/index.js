import React, { useContext } from 'react';
import { Table, Button } from 'react-bootstrap';
import Course from '../../components/Course';
import coursesData from '../../data/courses';
import UserContext from '../../UserContext';

export default function index() {

    // Use the UserContext and destructure it to access the user state defined in the App component
    const { user } = useContext(UserContext);

    // Create multiple Course components corresponding to the content of coursesData
    const courses = coursesData.map(course => {

        if(course.onOffer){
            return (
                <Course key={course.id} course={course}/>
            );
        }else{
            return null;
        }

    })

    // Table rows to be rendered in a bootstrap table when an admin is logged in
    const coursesRows = coursesData.map(course => {
        return (
            <tr key={course.id}>
                <td>{course.id}</td>
                <td>{course.name}</td>
                <td>PhP {course.price}</td>
                <td>{course.onOffer ? 'open': 'closed'}</td>
                <td>{course.start_date}</td>
                <td>{course.end_date}</td>
                <td>
                    <Button variant="warning">Update</Button>
                    <Button variant="danger">Disable</Button>
                </td>
            </tr>
        );
    })

    return (

        user.isAdmin === true
        ?
            <React.Fragment>
                <h1>Course Dashboard</h1> 
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {coursesRows}
                    </tbody>
                </Table>
            </React.Fragment>
        : 
            <React.Fragment> 
                {courses}
            </React.Fragment>
    )
}
