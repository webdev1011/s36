import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';

export default function index() {
    // Form input state hooks
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Function to simulate user registration
    function registerUser(e) {

        e.preventDefault();

        // Clear input fields
        setEmail('');
        setPassword1('');
        setPassword2('');

        console.log('Thank you for registering!');
    } 

    // Validate form input whenever email, password1, or password2 is changed
    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((email !== '' && password1 !== '' && password2 !== '') && (password2 === password1)){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password1, password2])

    return (
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email} 
                    onChange={e => setEmail(e.target.value)} 
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1} 
                    onChange={e => setPassword1(e.target.value)} 
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2} 
                    onChange={e => setPassword2(e.target.value)} 
                    required
                />
            </Form.Group>

            {/* Conditionally render submit button based on isActive state */}
            { isActive
                ? 
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                : 
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
            }
            
        </Form>
    )
}
